import asyncio

import ray



# We set num_cpus to zero because this actor will mostly just block on I/O.
@ray.remote(num_cpus=0)
class SignalActor:
    def __init__(self, init_val=0):
        self.ready_event = asyncio.Event()
        self.init_val = init_val

    def send(self, clear=False, init_val=0):
        self.init_val = init_val
        print(f"sending from s with init_val: {self.init_val}")
        self.ready_event.set()
        if clear:
            self.ready_event.clear()

    async def wait(self, should_wait=True, new_val=0):
        self.init_val = new_val
        if should_wait:
            await self.ready_event.wait()


@ray.remote
def wait_and_go(signal):
    ray.get(signal.wait.remote())

    print("go!")


signal = SignalActor.remote(5)
tasks = [wait_and_go.remote(signal) for n in range(4)]
print("ready...")
# Tasks will all be waiting for the signals.
print("set..")
ray.get(signal.send.remote(False, 2))

# Tasks are unblocked.
ray.get(tasks)

# Output is:
# ready...
# get set..

# (pid=77366) go!
# (pid=77372) go!
# (pid=77367) go!
# (pid=77358) go!