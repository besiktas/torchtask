from __future__ import print_function

import time

import argparse
from typing import Callable
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

import ray
from model import Net
# from torch.profiler import profile, record_function, ProfilerActivity

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()

    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()

        # with profile(activities=[ProfilerActivity.CPU, ProfilerActivity.CUDA], with_stack=True) as prof:
            # output = model(data)

        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print(
                "Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}".format(
                    epoch,
                    batch_idx * len(data),
                    len(train_loader.dataset),
                    100.0 * batch_idx / len(train_loader),
                    loss.item(),
                )
            )
            if args.dry_run:
                break



def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction="sum").item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print(
        "\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n".format(
            test_loss, correct, len(test_loader.dataset), 100.0 * correct / len(test_loader.dataset)
        )
    )


class Worker:
    def __init__(self, end_epoch: int = -1) -> None:
        self.end_epoch = end_epoch
        self.u_norm = None
        self.is_ready = False
        self.num_seen = 0
        self.type = None


    def time_intensive(self, vals: torch.Tensor):
        time0 = time.perf_counter()
        # vals = vals.flatten(1).repeat(3, 3)
        vals = vals.flatten(1)
        u, s, v = torch.svd(vals)
        self.u_norm = torch.norm(u)
        self.num_seen += len(vals)
        print(f"did time intensive...")

    def change_ready(self):
        self.is_ready = not self.is_ready

    def end_proc(self, epoch: int):
        if epoch == self.end_epoch:
            print(f"got end epoch and u norm: {self.u_norm}")

    def info(self):
        print(f"number seen: {self.num_seen}")


ray.init()

@ray.remote
class WorkerRemoteRay(Worker):
    def __init__(self, end_epoch: int = -1) -> None:
        super().__init__(end_epoch)

        self.type = "ray"

    def info(self):
        print(f"number seen: {self.num_seen}")

    def get_u_norm(self):
        return self.u_norm

    def get_u_norm_remote(self):
        # breakpoint()
        return self._get_u_norm.remote()

    def in_remote(self):
        print(f"in remote")

    def in_remote_remote(self):
        return self.in_remote.remote()
    # def _get_u_norm_remote(self):
    #     return





class WorkerLocal(Worker):
    def __init__(self, end_epoch: int = -1) -> None:
        super().__init__(end_epoch)


class Passer:

    def __init__(self, worker: Worker = None, send_remote: bool = False):
        self.activations = {}
        self.key = None
        self.worker = worker
        self.send_remote = send_remote

    def get_worker_method(self, func: Callable, /, *args, **kwargs):
        _worker_method = getattr(self.worker, func)
        if self.worker.type == "ray":
            return getattr(_worker_method, "remote")(*args, **kwargs)
        else:
            return _worker_method(*args, **kwargs)

    def attach(self, layer, module_name):
        self.key = module_name

        _layer_func = self.worker.time_intensive.remote if self.send_remote else self.worker.time_intensive
        transfer_values = lambda vals: vals[0].cpu()

        def _hook(module, inputs, outputs):
            _layer_func(transfer_values(inputs))


        layer.register_forward_hook(_hook)




    def call_func(self, actor, func, *args, **kwargs):
        if self.send_remote:
            return getattr(actor, func).remote()
        else:
            return getattr(actor, func)()

    def call_func_remote(self, actor):
        # actor.get_u_norm_remote()
        actor.in_remote_remote()




# class PasserRemote(Passer):
#     def __init__(self, worker: Worker = None, send_remote: bool = False):
#         super().__init__(worker, send_remote)


def main():
    # Training settings
    parser = argparse.ArgumentParser(description="PyTorch MNIST Example")
    parser.add_argument("--send-worker", default=False, action=argparse.BooleanOptionalAction)
    parser.add_argument(
        "--batch-size",
        type=int,
        default=2**10,
        metavar="N",
        help="input batch size for training (default: 2^10)",
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1000,
        metavar="N",
        help="input batch size for testing (default: 1000)",
    )
    parser.add_argument(
        "--epochs", type=int, default=5, metavar="N", help="number of epochs to train (default: 5)"
    )
    parser.add_argument("--lr", type=float, default=1.0, metavar="LR", help="learning rate (default: 1.0)")
    parser.add_argument(
        "--gamma", type=float, default=0.7, metavar="M", help="Learning rate step gamma (default: 0.7)"
    )
    parser.add_argument("--no-cuda", action="store_true", default=False, help="disables CUDA training")
    parser.add_argument("--dry-run", action="store_true", default=False, help="quickly check a single pass")
    parser.add_argument("--seed", type=int, default=1, metavar="S", help="random seed (default: 1)")
    parser.add_argument(
        "--log-interval",
        type=int,
        default=10,
        metavar="N",
        help="how many batches to wait before logging training status",
    )
    parser.add_argument(
        "--save-model", action="store_true", default=False, help="For Saving the current Model"
    )
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    train_kwargs = {"batch_size": args.batch_size}
    test_kwargs = {"batch_size": args.test_batch_size}
    if use_cuda:
        cuda_kwargs = {"num_workers": 1, "pin_memory": True, "shuffle": True}
        train_kwargs.update(cuda_kwargs)
        test_kwargs.update(cuda_kwargs)

    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))])
    dataset1 = datasets.MNIST("../data", train=True, download=True, transform=transform)
    dataset2 = datasets.MNIST("../data", train=False, transform=transform)
    train_loader = torch.utils.data.DataLoader(dataset1, **train_kwargs)
    test_loader = torch.utils.data.DataLoader(dataset2, **test_kwargs)

    model = Net().to(device)

    optimizer = optim.Adadelta(model.parameters(), lr=args.lr)

    watcher = WorkerRemoteRay.remote()
    passer = Passer(watcher, send_remote=args.send_worker)
    passer.attach(model.conv1, "conv1")



    scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)
    for epoch in range(1, args.epochs + 1):

        breakpoint()
        ray.get(passer.call_func_remote(watcher))
        train(args, model, device, train_loader, optimizer, epoch)
        test(model, device, test_loader)
        scheduler.step()

        # if epoch == args.epochs:
        #     watcher.change_ready.remote()

    if args.save_model:
        torch.save(model.state_dict(), "mnist_cnn.pt")

    print(f"info from this...")
    # watcher.info.remote()
    passer.call_func(watcher, "info")
    # vals_out = ray.get(watcher.get_u_norm.remote())
    # vals_out = ray.get(watcher.get_u_norm_remote())
    # vals_out = ray.get(passer.call_func(watcher, "get_u_norm"))
    vals_out = ray.get(passer.call_func_remote(watcher))
    print(f"got this val after: {vals_out}")



if __name__ == "__main__":
    time_0 = time.perf_counter()
    main()

    print(f"time to run all: {time.perf_counter() - time_0:.4f}")
