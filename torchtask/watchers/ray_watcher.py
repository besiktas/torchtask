import ray



@ray.remote
class WorkerWatcher:
    def __init__(self):
        # self.model = model

        self.activations = {}
        self.key = None

    def time_intensive(self, vals):
        print(f"got vals: {vals.shape}")
        # acts = self.activations[self.key][0].flatten(1)
        # acts = acts.repeat(3, 3)
        # u, s, v = torch.svd(acts)

    def info(self):
        return self.activations[self.key][0].shape


class Passer:
    def __init__(self, worker: WorkerWatcher = None, send_remote: bool = False):
        self.activations = {}
        self.key = None
        self.worker = worker
        self.send_remote = send_remote

    def attach(self, module, module_name):
        def _hook(_module, inputs, outputs):
            if self.send_remote:
                self.worker.time_intensive.remote(inputs[0].cpu().numpy())

        module.register_forward_hook(_hook)
        self.key = module_name