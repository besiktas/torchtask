import torch



class Watcher:
    def __init__(self) -> None:
        self.vals = {}

    def watch_model(self, model, method: str = "register_forward_hook"):
        def hook_fn(mod=None, input=None, output=None):
            self.vals[mod._name] += 1

        for name, module in model.named_modules():
            self.vals[name] = 0
            # if hasattr(module, "_name"):
            #     breakpoint()
            # else:
            #     setattr(module, "_name", name)
            setattr(module, "_name", name)
            getattr(module, method)(hook_fn)